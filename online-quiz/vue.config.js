const webpack = require('webpack')

module.exports = {
    devServer: {
        port: 8081,
        proxy: 'http://api.online-quiz.com/api/v1'
    },
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jquery: 'jquery',
                'window.jQuery': 'jquery',
                jQuery: 'jquery'
            })
        ]
    }
}