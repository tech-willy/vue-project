import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import NProgress from 'nprogress'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2';

import App from './App.vue'
import Home from './components/Home.vue'
import Create from './components/questions/Create.vue'
import Quiz from './components/quiz/Quiz.vue'
import Index from './components/Index.vue'

import './assets/css/fonts.googleapis.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'
import './assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'
import './assets/css/adminlte.min.css'
import '../node_modules/nprogress/nprogress.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(VueSweetalert2);
library.add(faSpinner)
// Vue.component('font-awesome', FontAwesomeIcon)

Vue.config.productionTip = false

const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home
    },
    {
        name: 'Create',
        path: '/create',
        component: Create
    },
    {
        name: 'Quiz',
        path: '/quiz',
        component: Quiz
    },
    {
        name: 'Posts',
        path: '/posts',
        component: Index
    }
];

const router = new VueRouter({ mode:'history', routes: routes });

router.beforeResolve((to, from, next) => {
    if( to.name ) {
        NProgress.start()
    }
    next()
});

router.afterEach(() => {
    NProgress.done()
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')